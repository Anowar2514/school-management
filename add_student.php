<?php
$message = '';
if (isset($_POST['btn'])) {
    require_once './student.php';
    $student = new Student();
    $message = $student->save_student_info($_POST);
}
if (isset($_POST['logout'])) {
    header('Location: home.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Admission</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Bashila Model School</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="dashboard.php">Dashboard</a></li>
                <li><a href="add_student.php">Admission</a></li>
                <li class="active"><a href="view_student.php">View Student</a></li>
                <li><a href="student_attendence.php">Student Attendance</a></li>
                <li class="active"><a href="teacher_attendence.php">Teacher Attendance</a></li>
                <li><a href="result.php">Result</a></li>
                <li class="active"><a href="invoice.php">Invoice</a></li>
            </ul>

            <form class="navbar-form navbar-left" method="post" action="" >
                <button type="submit" class="btn btn-default" name="logout">Log Out</button>
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="text-center text-success"><?php echo $message; ?></h3>
                    <hr/>
                    <div class="well">
                        <form class="form-horizontal" action="" method="post">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Student ID</label>
                                <div class="col-sm-10">
                                    <input type="text" name="s_id" class="form-control" placeholder="Write the student id">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Student Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="student_name" class="form-control" placeholder="Write the student name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Class</label>
                                <div class="col-sm-10">
                                    <input type="text" name="class" class="form-control" placeholder="Write the student class">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Roll</label>
                                <div class="col-sm-10">
                                    <input type="number" name="roll" class="form-control" placeholder="Write the student roll">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Section</label>
                                <div class="col-sm-10">
                                    <input type="text" name="section" class="form-control" placeholder="Write the student section">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" name="btn" class="btn btn-success btn-block">Save Student Info</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>