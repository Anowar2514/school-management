<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bashila Model School</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"  href="#">Bashila Model School</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a  href="home.php">Home</a></li>
                <li><a href="gallery.php">Gallery</a></li>
                <li class="active"><a href="notice.php">Notice</a></li>
                <li><a href="academic.php">Academic</a></li>
                <li class="active"><a href="about.php">About Us</a></li>
            </ul>
            <form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Search</button>
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="text-center text-success"><?php  ?></h3>
            <div class="well">
                <form class="form-horizontal" action="" method="post">
                        <img src="./image/home page.jpg" height="500px" width="100%" />
                </form>
                <h3 class="text-center text-success"><b>
                    A school is an institution designed to provide learning spaces and learning environments for  <br>
                    the teaching of students under the direction of teachers. Most countries<br>
                        have systems of formal education,which is commonly compulsory.<br>
                    </b>
                </h3>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="text-center text-success"><?php  ?></h3>
            <hr/>
            <div class="well">
                <form class="form-horizontal" action="" method="post">
                    <h1 class="text-center text-success" style="background-color:#122b40"> <b>LOG IN</b></h1>
                    <hr/><hr/>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Email or Phone</label>
                        <div class="col-sm-10">
                            <input type="email" name="email_or_phone" class="form-control" placeholder=" Write your email or phone">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" name="password" class="form-control" placeholder="Write your password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" name="login" class="btn btn-success btn-block">Log In</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
