<?php

class Student {

    protected $connection;

    public function __construct() {
        $host_name = 'localhost';
        $user_name = 'root';
        $password = '';
        $db_name = 'db_student_info';
        $this->connection = mysqli_connect($host_name, $user_name, $password, $db_name);
        if (!$this->connection) {
            die('Connection Fail' . mysqli_error($this->connection));
        }
    }

    public function save_student_info($data) {
        $sql = "INSERT INTO tbl_student (s_id, student_name, class, roll, section ) "
                . "VALUES ('$data[s_id]','$data[student_name]', '$data[class]', '$data[roll]', '$data[section]')";
        if (mysqli_query($this->connection, $sql)) {
            session_start();
            $_SESSION['message']='Student info saved successfully';
            header('Location: view_student.php');
        } else {
            die('Query problem' . mysqli_error($this->connection));
        }
    }

    public function select_all_student_info() {
        $sql = "SELECT * FROM tbl_student ORDER BY student_id DESC";
        if (mysqli_query($this->connection, $sql)) {
            $query_result = mysqli_query($this->connection, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->connection));
        }
    }
    public function select_student_info_by_id($student_id) {
        $sql = "SELECT * FROM tbl_student WHERE student_id = '$student_id' ";
        if (mysqli_query($this->connection, $sql)) {
            $query_result = mysqli_query($this->connection, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->connection));
        }
    }
    public function update_student_info($data) {
        $sql="UPDATE tbl_student SET s_id = '$data[s_id]', student_name = '$data[student_name]', class = '$data[class]', roll = '$data[roll]', section= '$data[section]' WHERE student_id= '$data[student_id]' ";
        if (mysqli_query($this->connection, $sql)) {
            session_start();
            $_SESSION['message']='Student info update successfully';
            header('Location: view_student.php');
        } else {
            die('Query problem' . mysqli_error($this->connection));
        }
    }
    public function delete_student_info($id) {
        $sql="DELETE FROM tbl_student WHERE student_id = '$id' ";
        if (mysqli_query($this->connection, $sql)) {
            $message='Student info delete successfully';
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->connection));
        }
    }

}
