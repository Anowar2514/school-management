<?php
if (isset($_POST['logout'])) {
    header('Location: home.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Teacher Attendance</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Bashila Model School</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="dashboard.php">Dashboard</a></li>
                <li><a href="add_student.php">Admission</a></li>
                <li class="active"><a href="view_student.php">View Student</a></li>
                <li><a href="student_attendence.php">Student Attendance</a></li>
                <li class="active"><a href="teacher_attendence.php">Teacher Attendance</a></li>
                <li><a href="result.php">Result</a></li>
                <li class="active"><a href="invoice.php">Invoice</a></li>
            </ul>

            <form class="navbar-form navbar-left" >
                <button type="submit" class="btn btn-default" name="logout">Log Out</button>
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="text-center text-success"><?php ?></h3>
            <hr/>
            <div class="well">
                <form class="form-horizontal" action="" method="post">

                </form>
            </div>
        </div>
    </div>
</div>

<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>