<?php
session_start();
$message = '';

require_once './student.php';
$student = new Student();

if(isset($_GET['delete'])) {
    $id = $_GET['delete'];
    $message = $student->delete_student_info($id);
}

if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
$query_result = $student->select_all_student_info();
if (isset($_POST['logout'])) {
    header('Location: home.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Student</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Bashila Model School</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="dashboard.php">Dashboard</a></li>
                <li><a href="add_student.php">Admission</a></li>
                <li class="active"><a href="view_student.php">View Student</a></li>
                <li><a href="student_attendence.php">Student Attendance</a></li>
                <li class="active"><a href="teacher_attendence.php">Teacher Attendance</a></li>
                <li><a href="result.php">Result</a></li>
                <li class="active"><a href="invoice.php">Invoice</a></li>
            </ul>

            <form class="navbar-form navbar-left" method="post" action="" >
                <button type="submit" class="btn btn-default" name="logout">Log Out</button>
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="text-center text-success"><?php echo $message; ?></h3>
                    <hr/>
                    <div class="well">
                        <table class="table table-bordered table-hover">
                            <tr>
                                <th>SL NO</th>
                                <th>Student ID</th>
                                <th>Student Name</th>
                                <th>Class</th>
                                <th>Roll</th>
                                <th>Section</th>
                                <th>Action</th>
                            </tr>
                            <?php $i = 1;
                            while ($student_info = mysqli_fetch_assoc($query_result)) {
                                ?>
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td contenteditable="true"><?php echo $student_info['s_id']; ?></td>
                                    <td><?php echo $student_info['student_name']; ?></td>
                                    <td><?php echo $student_info['class']; ?></td>
                                    <td><?php echo $student_info['roll']; ?></td>
                                    <td><?php echo $student_info['section']; ?></td>
                                    <td>
                                        <a href="edit_student.php?id=<?php echo $student_info['student_id']; ?>" class="btn btn-success" title="Edit">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>
                                        <a href="?delete=<?php echo $student_info['student_id']; ?>" class="btn btn-danger" title="Delete" onclick="return confirm('Are you sure to delete this !'); ">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                              <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>